import ciphers.Caesar
import ciphers.SDES
import ciphers.XOR
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.control.Button
import javafx.scene.image.Image
import javafx.scene.image.PixelReader
import javafx.scene.image.PixelWriter
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.stage.FileChooser
import tornadofx.*

class GUI : View("Ciphers") {
    override val root : VBox by fxml("ciphers.fxml")
    private val canvas : Canvas by fxid("canvas")
    private val buttonCaesar : Button by fxid("button_caesar")
    private val buttonXor : Button by fxid("button_xor")
    private val buttonSDES : Button by fxid("button_sdes")
    private val buttonChange : Button by fxid("button_change")
    private lateinit var image : Image
    private lateinit var gc : GraphicsContext
    private lateinit var pr : PixelReader
    private lateinit var pw : PixelWriter
    private lateinit var redArray : Array<Array<Double>>
    private lateinit var greenArray : Array<Array<Double>>
    private lateinit var blueArray : Array<Array<Double>>

    init {
        buttonCaesar.isDisable = true
        buttonXor.isDisable = true
        buttonSDES.isDisable = true

        buttonChange.setOnMouseClicked {
            val file = chooseFile("Select an image", Array<FileChooser.ExtensionFilter>(1) {FileChooser.ExtensionFilter("Image files (*.jpeg, *.bmp, *.png, *.gif)", "*.txt", "*.jpeg", "*.jpg", "*.bmp", "*.png", "*.gif")})
            image = Image(file.first().readBytes().inputStream())
            pr = image.pixelReader
            gc = canvas.graphicsContext2D
            pw = gc.pixelWriter
            redArray = Array(image.width.toInt()) {
                Array(image.height.toInt()) { 0.0 }
            }
            blueArray = Array(image.width.toInt()) {
                Array(image.height.toInt()) { 0.0 }
            }
            greenArray = Array(image.width.toInt()) {
                Array(image.height.toInt()) { 0.0 }
            }

            for(x in 0 until image.width.toInt()) {
                for(y in 0 until image.height.toInt()) {
                    redArray[x][y] = pr.getColor(x, y).red * 256
                    greenArray[x][y] = pr.getColor(x, y).green * 256
                    blueArray[x][y] = pr.getColor(x, y).blue * 256
                }
            }

            buttonCaesar.isDisable = false
            buttonXor.isDisable = false
            buttonSDES.isDisable = false

            drawImage()
        }

        buttonCaesar.setOnAction {
            if (buttonXor.isDisable) {
                buttonXor.isDisable = false
                buttonSDES.isDisable = false
                Caesar.decrypt(image, redArray, greenArray, blueArray)
            } else {
                buttonXor.isDisable = true
                buttonSDES.isDisable = true
                Caesar.encrypt(image, redArray, greenArray, blueArray)
            }
            drawImage()
        }

        buttonXor.setOnAction {
            if(buttonSDES.isDisable) {
                buttonCaesar.isDisable = false
                buttonSDES.isDisable = false
                XOR.decrypt(image, redArray, greenArray, blueArray)
            } else {
                buttonCaesar.isDisable = true
                buttonSDES.isDisable = true
                XOR.encrypt(image, redArray, greenArray, blueArray)
            }
            drawImage()
        }

        buttonSDES.setOnAction {
            if(buttonCaesar.isDisable) {
                buttonCaesar.isDisable = false
                buttonXor.isDisable = false
                SDES.decrypt(image, redArray, greenArray, blueArray)
            } else {
                buttonCaesar.isDisable = true
                buttonXor.isDisable = true
                SDES.encrypt(image, redArray, greenArray, blueArray)
            }
            drawImage()
        }
    }

    private fun drawImage() {
        gc.clearRect(0.0, 0.0, 1280.0, 660.0)

        for(x in 0 until image.width.toInt()) {
            for(y in 0 until image.height.toInt()) {
                val color = Color(redArray[x][y] / 256, greenArray[x][y] / 256, blueArray[x][y] / 256, 1.0)
                pw.setColor(x, y, color)
            }
        }
    }
}

class Ciphers : App() {
    override val primaryView = GUI::class
}