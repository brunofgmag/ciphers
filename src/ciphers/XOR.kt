package ciphers

import javafx.scene.image.Image
import kotlin.experimental.xor
import kotlin.math.roundToLong

object XOR : CipherInterface {
    private const val key = 0b11101110

    override fun encrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>) {
        for(x in 0 until input.width.toInt()) {
            for(y in 0 until input.height.toInt()) {
                redArray[x][y] = ((redArray[x][y].toLong().xor(key.toLong())).toDouble()) % 256
                greenArray[x][y] = ((greenArray[x][y].toLong().xor(key.toLong())).toDouble()) % 256
                blueArray[x][y] = ((blueArray[x][y].toLong().xor(key.toLong())).toDouble()) % 256
            }
        }
    }

    override fun decrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>) {
        encrypt(input, redArray, greenArray, blueArray)
    }
}