package ciphers

import javafx.scene.image.Image

interface CipherInterface {
    fun encrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>)
    fun decrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>)
}