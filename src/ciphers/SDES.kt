package ciphers

import ciphers.sdes.Key
import javafx.scene.image.Image
import java.nio.ByteBuffer
import kotlin.math.abs
import kotlin.math.roundToLong

object SDES : CipherInterface {
    private val key = Key(0b1010000010)
    private val s0 = Array(4) {
        Array(4) {1; 0; 3; 2}
        Array(4) {3; 2; 1; 0}
        Array(4) {0; 2; 1; 3}
        Array(4) {3; 1; 3; 2}
    }
    private val s1 = Array(4) {
        Array(4) {0; 1; 2; 3}
        Array(4) {2; 0; 1; 3}
        Array(4) {3; 0; 1; 0}
        Array(4) {2; 1; 0; 3}
    }

    private fun inputPermitation(input: Int) : Int {
        var ip = 0b00000000

        ip = ip.xor(key.getBit(input, 2, 8).shl(7))
        ip = ip.xor(key.getBit(input, 6, 8).shl(6))
        ip = ip.xor(key.getBit(input, 3, 8).shl(5))
        ip = ip.xor(key.getBit(input, 1, 8).shl(4))
        ip = ip.xor(key.getBit(input, 4, 8).shl(3))
        ip = ip.xor(key.getBit(input, 8, 8).shl(2))
        ip = ip.xor(key.getBit(input, 5, 8).shl(1))
        ip = ip.xor(key.getBit(input, 7, 8).shl(0))

        return ip
    }

    private fun expandPermutate(input: Int) : Int {
        var op = 0b00000000

        op = op.xor(key.getBit(input, 4, 4).shl(7))
        op = op.xor(key.getBit(input, 1, 4).shl(6))
        op = op.xor(key.getBit(input, 2, 4).shl(5))
        op = op.xor(key.getBit(input, 3, 4).shl(4))
        op = op.xor(key.getBit(input, 2, 4).shl(3))
        op = op.xor(key.getBit(input, 3, 4).shl(2))
        op = op.xor(key.getBit(input, 4, 4).shl(1))
        op = op.xor(key.getBit(input, 1, 4).shl(0))

        return op
    }

    private fun p4(input: Int) : Int {
        var p4 = 0b0000

        p4 = p4.xor(key.getBit(input, 2, 4).shl(3))
        p4 = p4.xor(key.getBit(input, 4, 4).shl(2))
        p4 = p4.xor(key.getBit(input, 3, 4).shl(1))
        p4 = p4.xor(key.getBit(input, 1, 4).shl(0))

        return p4
    }

    private fun swap(input: Int) : Int{
        var l = 0b0000
        var r = 0b0000

        for(i in 0..3) {
            r = r.xor(key.getBit(input, (4 + i)).shl(i))
        }

        for(i in 0..3) {
            l = l.xor(key.getBit(input, i).shl(i))
        }

        return l.shl(4).xor(r)
    }

    private fun inversePermutate(input: Int) : Int {
        var ipInv = 0b00000000

        ipInv = ipInv.xor(key.getBit(input, 4, 8).shl(7))
        ipInv = ipInv.xor(key.getBit(input, 1, 8).shl(6))
        ipInv = ipInv.xor(key.getBit(input, 3, 8).shl(5))
        ipInv = ipInv.xor(key.getBit(input, 5, 8).shl(4))
        ipInv = ipInv.xor(key.getBit(input, 7, 8).shl(3))
        ipInv = ipInv.xor(key.getBit(input, 2, 8).shl(2))
        ipInv = ipInv.xor(key.getBit(input, 8, 8).shl(1))
        ipInv = ipInv.xor(key.getBit(input, 6, 8).shl(0))

        return ipInv
    }

    private fun sdesProcedure(input: Int, k: Int) : Int {
        var ipl = 0b0000
        var ipr = 0b0000

        for(i in 0..3) {
            ipl = ipl.xor(key.getBit(input, (4 + i)).shl(i))
        }

        for(i in 0..3) {
            ipr = ipr.xor(key.getBit(input, i).shl(i))
        }

        val ep = expandPermutate(ipr)

        var xorlh = 0b0000
        var xorrh = 0b0000
        val xor = ep.xor(k)

        for(i in 0..3) {
            xorlh = xorlh.xor(key.getBit(xor, (4 + i)).shl(i))
        }

        for(i in 0..3) {
            xorrh = xorrh.xor(key.getBit(xor, i).shl(i))
        }

        var rowlh = 0b00
        rowlh = rowlh.xor(key.getBit(xorlh, 1, 4).shl(1))
        rowlh = rowlh.xor(key.getBit(xorlh, 4, 4).shl(0))

        var columnlh = 0b00
        columnlh = columnlh.xor(key.getBit(xorlh, 2, 4).shl(1))
        columnlh = columnlh.xor(key.getBit(xorlh, 3, 4).shl(0))

        var rowrh = 0b00
        rowrh = rowrh.xor(key.getBit(xorrh, 1, 4).shl(1))
        rowrh = rowrh.xor(key.getBit(xorrh, 4, 4).shl(0))

        var columnrh = 0b00
        columnrh = columnrh.xor(key.getBit(xorrh, 2, 4).shl(1))
        columnrh = columnrh.xor(key.getBit(xorrh, 3, 4).shl(0))

        val s0output = s0[rowlh][columnlh]
        val s1output = s1[rowrh][columnrh]

        val join = s0output.shl(2).xor(s1output)

        val p4 = p4(join)

        val p4xor = p4.xor(ipl)

        return p4xor.shl(4).xor(ipr)
    }

    private fun encrypt(input: Int): Int {
        val k1 = key.k1()
        val k2 = key.k2()
        var data = sdesProcedure(inputPermitation(input.toInt()), k1)
        data = swap(data)

        return inversePermutate(sdesProcedure(data, k2))
    }

    private fun decrypt(input: Int): Int {
        return encrypt(input)
    }

    override fun encrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>) {
        for(x in 0 until input.width.toInt()) {
            for(y in 0 until input.height.toInt()) {
                redArray[x][y] = encrypt(redArray[x][y].toInt()).toDouble()
                greenArray[x][y] = encrypt(greenArray[x][y].toInt()).toDouble()
                blueArray[x][y] = encrypt(blueArray[x][y].toInt()).toDouble()
            }
        }
    }

    override fun decrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>) {
        for(x in 0 until input.width.toInt()) {
            for(y in 0 until input.height.toInt()) {
                redArray[x][y] = decrypt(redArray[x][y].toInt()).toDouble()
                greenArray[x][y] = decrypt(greenArray[x][y].toInt()).toDouble()
                blueArray[x][y] = decrypt(blueArray[x][y].toInt()).toDouble()
            }
        }
    }
}