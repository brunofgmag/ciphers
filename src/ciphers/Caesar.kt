package ciphers

import javafx.scene.image.Image

object Caesar : CipherInterface {
    private const val shift = 99

    override fun encrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>) {
        for(x in 0 until input.width.toInt()) {
            for(y in 0 until input.height.toInt()) {
                redArray[x][y] = (redArray[x][y] + shift) % 256
                greenArray[x][y] = (greenArray[x][y] + shift) % 256
                blueArray[x][y] = (blueArray[x][y] + shift) % 256
            }
        }
    }

    override fun decrypt(input: Image, redArray: Array<Array<Double>>, greenArray: Array<Array<Double>>, blueArray: Array<Array<Double>>) {
        for(x in 0 until input.width.toInt()) {
            for(y in 0 until input.height.toInt()) {
                redArray[x][y] = (redArray[x][y] + 256 - shift % 256) % 256
                greenArray[x][y] = (greenArray[x][y] + 256 - shift % 256) % 256
                blueArray[x][y] = (blueArray[x][y] + 256 - shift % 256) % 256
            }
        }
    }
}