package ciphers.sdes

class Key(key : Int) {
    private var p10 = 0b0000000000
    private var p5l1 = 0b00000
    private var p5r1 = 0b00000
    private var p5l2 = 0b00000
    private var p5r2 = 0b00000

    init {
        p10 = p10.xor(getBit(key, 3, 10).shl(9))
        p10 = p10.xor(getBit(key, 5, 10).shl(8))
        p10 = p10.xor(getBit(key, 2, 10).shl(7))
        p10 = p10.xor(getBit(key, 7, 10).shl(6))
        p10 = p10.xor(getBit(key, 4, 10).shl(5))
        p10 = p10.xor(getBit(key, 10, 10).shl(4))
        p10 = p10.xor(getBit(key, 1, 10).shl(3))
        p10 = p10.xor(getBit(key, 9, 10).shl(2))
        p10 = p10.xor(getBit(key, 8, 10).shl(1))
        p10 = p10.xor(getBit(key, 6, 10).shl(0))
    }

    fun getBit(input : Int, position : Int) : Int {
        return input.shr(position).and(1)
    }

    fun getBit(input : Int, position : Int, size: Int) : Int {
        return input.shr(size - position).and(1)
    }

    fun k1() : Int {
        if(p5l1 != 0b00000 && p5r1 != 0b00000) {
            return p8(p5l1, p5r1)
        }

        for(i in 0..4) {
            p5l1 = p5l1.xor(getBit(p10, (5 + i)).shl(i))
        }
        val lsb5l = p5l1.shr(4).and(1)
        p5l1 = p5l1.shl(1).xor(lsb5l).and(lsb5l.shl(5).inv())

        for(i in 0..4) {
            p5r1 = p5r1.xor(getBit(p10, i).shl(i))
        }
        val lsb5r = p5r1.shr(4).and(1)
        p5r1 = p5r1.shl(1).xor(lsb5r).and(lsb5r.shl(5).inv())

        return p8(p5l1, p5r1)
    }

    fun k2() : Int {
        if(p5l2 != 0b00000 && p5r2 != 0b00000) {
            return p8(p5l2, p5r2)
        }
        val lsb5l1 = p5l1.shr( 3).and(1)
        val lsb5l2 = p5l1.shr(4).and(1)
        p5l2 = p5l1.shl(1).xor(lsb5l1).and(lsb5l1.shl(5).inv())
        p5l2 = p5l2.shl(1).xor(lsb5l2).and(lsb5l2.shl(5).inv())

        val lsb5r1 = p5r1.shr(3).and(1)
        val lsb5r2 = p5r1.shr(4).and(1)
        p5r2 = p5r1.shl(1).xor(lsb5r1).and(lsb5r1.shl(5).inv())
        p5r2 = p5r2.shl(1).xor(lsb5r2).and(lsb5r2.shl(5).inv())

        return p8(p5l2, p5r2)
    }

    private fun p8(p5l: Int, p5r: Int) : Int {
        var p8 = 0b00000000
        val p10s = p5r.xor(p5l.shl(5))

        p8 = p8.xor(getBit(p10s, 6, 10).shl(7))
        p8 = p8.xor(getBit(p10s, 3, 10).shl(6))
        p8 = p8.xor(getBit(p10s, 7, 10).shl(5))
        p8 = p8.xor(getBit(p10s, 4, 10).shl(4))
        p8 = p8.xor(getBit(p10s, 8, 10).shl(3))
        p8 = p8.xor(getBit(p10s, 5, 10).shl(2))
        p8 = p8.xor(getBit(p10s, 10, 10).shl(1))
        p8 = p8.xor(getBit(p10s, 9, 10).shl(0))

        return p8
    }
}